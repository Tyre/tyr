# What is this?

This is an experimental minimalistic language.
It's meant to be both a programming language and a conlang.

Programmers should think of it as a speakable programming language.
Conlangers should see it as a logical conlang, which can also be used for programming.

To achieve this, this language has clear and simple nesting rules based on the words, but no syntax for nesting.
You can write sentences without the need of indentation. Any kind of indent is not even allowed.

It's still in an early stage and only supports a few math primitives.
Many things will probably be changed. I'll only document the current state here.

# Getting started

Simply install this program using [cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html).

Then just use it this way:

```
cargo install --git "https://gitlab.com/Tyre/tyr"
```

You can also just run the program inside the directory directly after cloning it using `cargo run`.

You might want to have a look at the examples in this document if you just want to try it.

# Language

## Phonology

### Vowels

* a: /a/
* i: /i/
* u: /u/

### Consonants

#### Approximants

* v: /w/
* j: /j/

#### Liquids

* l: /l/

#### Nasals

* m: /m/
* n: /n/

#### Fricatives

* f: /f/
* s: /s/
* z: /ʃ/
* h: /x/

### Stops

* b: /b/, /p/
* c: /g/, /k/
* d: /d/, /t/

## Phonotactics

Every word consists of a single syllable.

It consists of a prefix, a single vowel and a suffix.
Both the prefix and the suffix can contain multiple consonants.

```
C*VC*
```

Both the prefix and the suffix have specific simple phonotactics, which are different from each other. 

For simplification, some different kinds of phonemes will be grouped:

- `M` => `N|L`
- `E` => `A|L`
- `C` => `F|N`

The phonotactics of the prefix and suffix look like this:

- prefix: `F?M?A?`
- suffix: `E?C?`


So the phonotactics of a word looks like this:

```
S?F?M?A?VE?C?
```

Besides that, every word needs to contain at least one consonant.

## Syntax

A text can be separated into sentences by using separators.
All of these symbols can be used as separators: `,`, `;`, `:`, `.`, `!`, `?`.

Words are implicitly separated based on the phonotactics.
Consonants always belong to the following word.
Except, if there is a stop inside a sequence of consonants, the consonants before the stop will belong to the previous word.

For example if a sentence contains `C*VC*VC*` and none of the consonants is a stop, then it represents two words: `C*V` and `C*VC*`.
But if a sentence contains stops like this `C*VC*SC*VC*` it represents these two words: `C*VC*` and `SC*VC*`.

In order to delimit words manually, it's possible to use delimiters.
Delimiters can be a whitespace, a newline or an apostrophe (`'`).

It's not allowed to use multiple consecutive delimiters.
After a separator, there always needs to be exactly one delimiter.

## Grammar

here are three types of words:

* objects
* specifiers
* relations

Each word kind has specific phonotactics.

### Objects

Objects represent an object by themselves.

An object is enough to form a sentence.
After the object, the sentence or subsentence ends.

If the sentence ends, the begin of a new sentence always needs to be explicity using delimiters.

Object always have both a prefix and a suffix (`C+VC+`).

### Specifiers

Specifiers change the meaning of an object.
They can be written before an object to change its meaning.

They act like single argument functions or adjectives in western languages.
Multiple of them are applied from right to left.

```
S O => S(O)
S1 S2 S3 O => S1(S2(S3(O)))
```

Specifiers always have just a prefix, no suffix (`C+V`).

### Relations

Relations, similar to specifiers, change the meaning of an object.
But they also apply to a second object.

The second object is written directly behind the relation. The object can also represented by a full subsentence.
But after the object, the main object will follow.

They are similar to two argument functions or to subclauses in western languages.
There's a strict nesting. For each relation, there needs to be an object.
Since relations just act like specifiers, they can just be mixed.

```
R O1 O2 => R(O1, O2)
R1 O1 R2 O2 R3 O3 O4 => R1(O1, R2(O2, R3(O3, O4)))
R S O1 O2 => R(S(O1), O2)
R1 R2 R3 O1 O2 O3 O4 => R1(R2(R3(O1, O2), O3), O4)
S1 R1 S2 O2 S3 R2 O3 O4  => S1(R1(S2(O2), S3(R2(O3, O4))))
```

Relations always have just a suffix, no prefix (`VC+`).

### Modifiers

Modifiers change the meaning of the word before them.
So they can be written after any word to chenge its meaning.

If it's writen after an object, though, it doesn't change the meaning of the object, else it would just be a special modifier.
In this case, it's interpreted as changing the meaning of the specifier represented by the specifier.

They can't be after an object without being behind an object, which is not part of a subsentence, since behind this object, the sentence will always be over.
They also can't be the first word in a sentence.

They act similar to suffixes in western languages.
Multiple of them are applied in order.

Their meaning often depends on the word they are applied to and can't necessarily be desribed in a general way.
Existing words have to be applyable to both, specifiers and relations, but they are allowed to produce values representing unknown objects.

```
S M O => M(S)(O)
R M O1 O2 => M(R)(O1, O2)
R O1 M O2 => M(R(O1))(O2)
R M1 O1 M2 S M3 O2 => M2(M1(R)(O1))(M3(S)(O2))
```

Modifiers always start with a stop (`SC*VC*`).

## Words

### Objects

- `nan`: 0 (number)
- `van`: 1 (number, short for `vinan`)
- `zan`: 6 (number, short `zavan`, which is short for `zavinan`)

### Specifiers

- `vi`: increase (increases the object by one)
- `ni`: increase 2 (increases the object by two)
- `li`: increase 3 (increases the object by three)
- `nu`: decrease 2 (decreases the object by two)
- `vu`: decrease (decreases the object by one)
- `ju`: negate (negates the object)
- `sa`: inverse (takes the multiplicative inverse of the object)
- `za`: multiply by 6
- `sla`: multiply by niftimal 10
- `zi`: multiply by niftimal 100
- `sli`: multiply by niftimal 10000
- `zu`: multiply by niftimal 100000000
- `slu`: multiply by niftimal 10000000000000000

### Relations

- `an`: add (adds some object to the object)
- `un`: subtract (substracts some object from the object)
- `ul`: multiply (multiplies some object by the object)
- `il`: divide (divides some object by the object)

### Modifiers

- `da`: opposite (might turn addition into substraction, and similar things)
- `ba`: passive (switches the argument order for relations, does something similar for specifiers)

## Numbers

Numbers are represented as niftimal and constructed using simple mathematical operations.

Here some examples:

- 0: nan
- 1: van (vinan)
- 2: ninan
- 3: linan
- 4: nuzan (nuzavan)
- 5: vuzan (vuzavan)
- 6: zan (zavan)
- 7: vizan
- 8: nizan
- 9: lizan
- A: nuzaninan
- B: vuzaninan
- C: zaninan
- D: vizaninan
- E: nizaninan
- F: lizaninan
- G: nuzalinan
- I: zalinan
- O: zanuzan
- U: zavuzan
- X: lizavuzan
- Y: nuslavan (nuzazan)
- Z: vuslavan (vuzazan)
- 10: slavan (zazan)
- 20: slaninan
- 30: slalinan
- 40: slanuzan
- 50: slavuzan
- 60: slazan
- 90: slalizan
- C0: slazaninan
- ZZ: vuzivan
- 100: zivan (slaslavan)
- 600: zizan
- 1000: zislavan
- 6000: zislazan
- 10000: slivan (zizivan)
- 100000: slislavan
- 1000000: slizivan
- 10000000: slizislavan
- 100000000: zuvan (slislivan)
- 10000000000: zuzivan
- 1000000000000: zuslivan
- 10000000000000000: sluvan (zuzuvan)
- 1000000000000000000000000: sluzuvan

# Example

This is an example session:

```
* van
=> 1

* van, ninan, linan!
=> 1, 2, 3

* vi van
=> 2

* ju van
=> -1

* vi ju van
=> 0

* ju vi van
=> -2

* il vuzan un nuzan ul linan an ninan vu sa ju vi van
=> -1/2

* vu il vuzan sa un nuzan ju ul linan vi an ninan van
=> 29/28

* il vu vuzan un sa nuzan ul ju linan an vi ninan van
=> -1D/G
```

You should try to write without using delimiters, where possible.

This is the same session, but without using spaces and apostrophes as delimiters only where necessary.

```
* van
=> 1

* van, ninan, linan!
=> 1, 2, 3

* vivan
=> 2

* juvan
=> -1

* vijuvan
=> 0

* juvivan
=> -2

* il'vuzan'un'nuzan'ul'linan'an'ninan'vusajuvivan
=> -1/2

* vuil'vuzan'saun'nuzan'juul'linan'vian'ninan'van
=> 29/28

* il'vuvuzan'un'sanuzan'ul'julinan'an'vininan'van
=> -1D/G
```

If you make mistakes, you get helpful error messages.

For example, you might want to write down numbers from one to three:

```
* 
!! Delimiter not after word at 0:0!

* van, ninan, !
!! The sentence at 0:12 does not contain any words!

* van,  ninan,  linan!
!! Delimiter not after word at 0:5!

* van , ninan , linan!
!! Separator after delimiter at 0:4!

* van,ninan,linan!
!! Missing delimiter after separator at 0:4!

* a,u,i!
!! Invalid phonotactics for word 'a' at 0:1!

* van, niinan, linan!
!Invalid phonotactics for word 'i' at 0:9!

* van, ninan, rinen!
!! Character 'r' at 0:12 is not allowed!

* van ninan linen
!! Character 'e' at 0:13 is not allowed!

* van ninan linan
!! The sentence still has words after the last word: ni, nan, li, nan

* ul van
!! Sentence is not finished at level 0!

* ul an il linan
!! Sentence is not finished at level 2!

* van, niman, linan!
!! The object word 'man' does not exist in this language!

* vaan, ninan, linan!
!! The specifier word 'va' does not exist in this language!

* un van am ninan linan
!! The relation word 'am' does not exist in this language!

* van, ninan, linan!
=> 1, 2, 3
```

# Planned features

* running from file
* splitting words totally based on phonotactics to avoid explicit delimiters wherever possible
* defining your own words
* some kind of object system, not just numbers
* words to refer to already mentioned objects, to be used instead of local variables in other languages
* markdown inspired headers to define submodules
* add more useful modifiers
* efficient compiliation

