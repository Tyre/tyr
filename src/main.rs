use tyr_core::{Entity, Evaluable, Property};
use tyr_grammar::{
    MathOperation, Num, ObjectSet, Operation, SetOperation, TyrItem, TyrModifier, TyrObject,
    TyrRelation, TyrSpecifier,
};
use tyr_syntax::{parse, Dictionary};

use radix_fmt::radix_36;

use std::collections::BTreeMap;

use std::io::{self, Write};

fn default_dictionary() -> Dictionary<TyrObject, TyrSpecifier, TyrRelation, TyrModifier> {
    fn n(n: i128) -> TyrObject {
        TyrObject::new(TyrItem::Number(Num::Rational(n.into())))
    }

    let mut objects = BTreeMap::new();
    objects.insert("nan".into(), n(0));
    objects.insert("van".into(), n(1));
    objects.insert("zan".into(), n(6));

    let [add, sub, mul, div, pow, root]: [Operation; 6] = {
        use MathOperation::*;
        [
            Add.into(),
            Sub.into(),
            Mul.into(),
            Div.into(),
            Pow.into(),
            Root.into(),
        ]
    };

    let [union, intersection]: [Operation; 2] = {
        use SetOperation::*;
        [Union.into(), Intersection.into()]
    };

    let mut specifiers = BTreeMap::new();
    for (i, name) in ["vi", "ni", "li"].into_iter().enumerate() {
        specifiers.insert(name.into(), add.specifier(n(1 + i as i128)));
    }
    for (i, name) in ["vu", "nu"].into_iter().enumerate() {
        specifiers.insert(name.into(), sub.specifier(n(1 + i as i128)));
    }
    specifiers.insert("ju".into(), sub.specifier(n(0)).passive());
    specifiers.insert("sa".into(), div.specifier(n(1)).passive());
    let mut s = 6;
    for name in ["za", "sla", "zi", "sli", "zu", "slu"] {
        specifiers.insert(name.into(), mul.specifier(n(s)));
        s *= s;
    }

    let mut relations = BTreeMap::new();
    relations.insert("an".into(), add.relation());
    relations.insert("un".into(), sub.relation());
    relations.insert("ul".into(), mul.relation());
    relations.insert("il".into(), div.relation());
    relations.insert("uvh".into(), pow.relation());
    relations.insert("ivh".into(), root.relation());
    relations.insert("avm".into(), union.relation());
    relations.insert("uvm".into(), intersection.relation());

    let mut modifiers = BTreeMap::new();
    modifiers.insert("da".into(), TyrModifier::Opposite);
    modifiers.insert("ba".into(), TyrModifier::Passive);

    Dictionary::new(objects, specifiers, relations, modifiers)
}

fn print_item(item: TyrItem) {
    use TyrItem::*;
    match item {
        Number(num) => {
            print!("#");

            use Num::*;
            match num {
                Rational(num) => {
                    let (mut numer, denom) = num.into();
                    if numer < 0 {
                        print!("-");
                        numer = -numer;
                    }
                    print!("{:#}", radix_36(numer));
                    if denom != 1 {
                        print!("/{:#}", radix_36(denom));
                    }
                }
                Irrational => print!("???"),
            }
        }
        Unknown => print!("???"),
    }
}

fn print_eval_sentence(
    sentence: Entity<Property<TyrObject, TyrSpecifier, TyrRelation, TyrModifier>, TyrObject>,
) {
    let ObjectSet(items) = sentence.eval();
    let mut items = items.into_iter();
    if let Some(item) = items.next() {
        print_item(item);

        for item in items {
            print!("&");
            print_item(item);
        }
    } else {
        print!("nothing");
    }
}

fn main() {
    let dictionary = default_dictionary();

    let mut buffer = String::new();

    let stdin = io::stdin();
    let mut stdout = io::stdout();

    loop {
        print!("* ");
        stdout.flush().unwrap();
        stdin.read_line(&mut buffer).expect("Input Error");

        let sentences = parse(buffer.chars(), &dictionary);

        match sentences {
            Ok(sentences) => {
                print!("=> ");
                let mut sentences = sentences.into_iter();
                if let Some(sentence) = sentences.next() {
                    print_eval_sentence(sentence);
                    for sentence in sentences {
                        print!(", ");
                        print_eval_sentence(sentence);
                    }
                    println!();
                }
            }
            Err(err) => println!("!! {err}"),
        }

        println!();

        buffer.clear();
    }
}
